/**
 * Created by scarzer on 5/10/16.
 */
console.log("Hello World");
var container = document.getElementById('networkGraphColumn');
var options = {
    interaction : {
        dragNodes : true,
        hover : true
    },
    edges: {
        "smooth": {
            "type": "cubicBezier",
            "forceDirection": "vertical",
            "roundness": .75
        }
    }, 
    physics : false,

};

var ourStory = {
    '118149' : "In State Oil V. Khan, years of Anti-Trust litigation were overruled when Vertical Price Fixing was given a new latitude. " +
    "Despite the young age of this case, several cases have already taken advantage of this decision"
};


/*
 getRelationships(145701).then( (result) => {
    nodes.add({
        id : result['original_case']['file'],
        label : result['original_case']['case_name'],
        title : result['original_case']['filed'],
        shape : "box",
        level : 1
    });
    for(var i = 0; i < result['cited_cases'].length; ++i){
        nodes.add({
            id : result['cited_cases'][i]['file'],
            label : result['cited_cases'][i]['name'],
            title : result['cited_cases'][i]['filed'],
            shape : "box",
            level : 2
        });
        edges.add({
            to : result['cited_cases'][i]['file'],
            from: result['original_case']['file'],
            arrows: 'middle'
        })
    }
    
    for(var j = 0; j < result['cites_cases'].length; ++j){
        nodes.add({
            id: result['cites_cases'][j]['file'],
            label: result['cites_cases'][j]['name'],
            title : result['cites_cases'][j]['filed'],
            shape : "box",
            level : 0
        });
        edges.add({
            from : result['cites_cases'][j]['file'],
            to: result['original_case']['file'],
            arrows: 'middle'
        })
        
    }
});
*/

// Case file numbers for the cases that we're gonna be working with
var cases_of_interest = [
    145701,     // Leegin Creative Leather Products v. PSKS, Inc
    118149,     // State Oil Co. v. Khan
    107615,     // Albrecht v. Herald Co
    104825,     // Standard Oil of New Jersey
    94079      // United States v. EC Knight Co
];

var nodes = new vis.DataSet();
var edges = new vis.DataSet();
var data = {
    nodes : nodes,
    edges : edges
};

// Counter to make columns
var casesInserted = 1;

cases_of_interest.forEach( (val, index) => {
    getRelationships(val).then( (result) => {
        nodes.update({
            id: val,
            label: result['original_case']['case_name'],
            mainCase: true,
            color: "rgb(97,195,238)",
            shape : "box",
            physics : "true",
            x : 0,
            y : casesInserted * 10
        });
         result['cites_cases'].forEach(  (cites, ind) => {
             var cited_case_node = {
                 id: cites.file,
                 label: cites.name,
                 mainCase: false,
                 shape: "text",
                 x: Math.pow(-1, casesInserted) * 400,
                 y: (Math.pow(-1, casesInserted) * 10) * casesInserted
             };
             try {
                 cited_case_node.x = Math.pow(-1, casesInserted) * 400;
                 cited_case_node.y = (Math.pow(1, casesInserted) * 10) * casesInserted;
                 nodes.add(cited_case_node);
                 casesInserted++;
             } catch (e) {}
             
             edges.add({
                 from: val,
                 to : cites.file,
                 arrows : "middle",
                 color : "rgba(0,0,255,0.5)"
             })
         });
 
        result['cited_cases'].forEach( (cited, ind) => {
            var cited_case_node = {
                id: cited.file,
                label: cited.name,
                mainCase: false,
                shape: "text",
                x: Math.pow(-1, casesInserted) * 400,
                y: (Math.pow(-1, casesInserted) * 10) * casesInserted,
                fixed : {
                    x: true,
                    y: true
                }
            };
            
            try {
                cited_case_node.x = Math.pow(-1, casesInserted) * 400;
                cited_case_node.y = (Math.pow(1, casesInserted) * 10) * casesInserted;
                nodes.add(cited_case_node);
                casesInserted++;
            } catch (e) {}
            
            edges.add({
                from: val,
                to : cited.file,
                arrows : "middle",
                color : "rgba(255,0,0,0.5)",
                arrowStrikethrough : true
            })
        });
   });
});

var network = new vis.Network(container, data, options);
network.on('click', (params) => {
    
    if(params.nodes[0] === undefined) return;
    var clickedNode = nodes.get(params.nodes[0]);
    if(clickedNode['clicked'] === false || clickedNode['clicked'] === undefined) {
        clickedNode['clicked'] = true;
    } else{
        var allNodes = nodes.get();
        var allEdges = edges.get();
        
        for(edge in allEdges) {
            var tempEdge = allEdges[edge];
            tempEdge.hidden = false;
            edges.update(tempEdge);
        }
        for(node in allNodes){
            var tempNode = allNodes[node];
            tempNode.hidden = false;
            tempNode.clicked = false;
            nodes.update(tempNode);
        }
   }
    var selectedNodes = [params.nodes[0]];
    var selectedNodes = selectedNodes.concat(network.getConnectedNodes(params.nodes[0]));

    var wontBeVisible = nodes.get({
        filter : function(item){
            return (selectedNodes.indexOf(item.id) === -1);
        }
    });
    for(var n in wontBeVisible){
        if(wontBeVisible.hasOwnProperty(n)) {
            var tempNode = wontBeVisible[n];
            var tempEdges = network.getConnectedEdges(tempNode['id']);
            tempEdges.map( (val) => {
                var theEdge = edges.get(val); 
                theEdge.hidden = true;
                edges.update(theEdge);
            });
            
            tempNode.hidden = true;
            nodes.update(tempNode);
        }
    }

});


network.on('doubleClick', function (params){
    nodes.forEach(function(node){
        node.hidden = false;
        nodes.update(node);
    });
    edges.forEach(function(edge){
        edge.hidden = false;
        edges.update(edge);
    })
});


function getNode(fileNumber){
    return $.get('/api/node/' + fileNumber);
}

function getRelationships(fileNumber){
    return $.get('/api/relationships/' + fileNumber)
}

function neighbourhoodHighlight(params) {
    // if something is selected:
    if (params.nodes.length > 0) {
        highlightActive = true;
        var i,j;
        var selectedNode = params.nodes[0];
        var degrees = 2;

        // mark all nodes as hard to read.
        for (var nodeId in nodes) {
            nodes[nodeId].color = 'rgba(200,200,200,0.5)';
            if (nodes[nodeId].hiddenLabel === undefined) {
                nodes[nodeId].hiddenLabel = nodes[nodeId].label;
                nodes[nodeId].label = undefined;
            }
        }
        var connectedNodes = network.getConnectedNodes(selectedNode);
        var allConnectedNodes = [];

        // get the second degree nodes
        for (i = 1; i < degrees; i++) {
            for (j = 0; j < connectedNodes.length; j++) {
                allConnectedNodes = allConnectedNodes.concat(network.getConnectedNodes(connectedNodes[j]));
            }
        }

        // all second degree nodes get a different color and their label back
        for (i = 0; i < allConnectedNodes.length; i++) {
            nodes.get(allConnectedNodes[i]).color = 'rgba(150,150,150,0.75)';
            if (nodes.get(allConnectedNodes[i]).hiddenLabel !== undefined) {
                nodes.get(allConnectedNodes[i]).hiddenLabel;
                nodes.get(allConnectedNodes[i]).hiddenLabel = undefined;
            }
        }

        // all first degree nodes get their own color and their label back
        for (i = 0; i < connectedNodes.length; i++) {
            nodes.get(connectedNodes[i]).color = undefined;
            if (nodes.get(connectedNodes[i]).hiddenLabel !== undefined) {
                nodes.get(connectedNodes[i]).hiddenLabel;
                nodes.get(connectedNodes[i]).hiddenLabel = undefined;
            }
        }

        // the main node gets its own color and its label back.
        nodes.get(selectedNode[i]).color = undefined;
        if (nodes.get(selectedNode[i]).hiddenLabel !== undefined) {
            nodes.get(selectedNode[i]).hiddenLabel;
            nodes.get(selectedNode[i]).hiddenLabel = undefined;
        }
    }
    else if (highlightActive === true) {
        // reset all nodes
        for (var nodeId in nodes) {
            nodes.get(nodeId).color = undefined;
            if (nodes.get(nodeId).hiddenLabel !== undefined) {
                nodes.get(nodeId).hiddenLabel;
                allNode[nodeId].hiddenLabel = undefined;
            }
        }
        highlightActive = false
    }

    // transform the object into an array
    var updateArray = [];
    for (nodeId in nodes["_data"]){
        if (nodes.hasOwnProperty(nodeId)) {
            updateArray.push(nodes.get(nodeId));
        }
    }
    nodes.update(updateArray);
}

