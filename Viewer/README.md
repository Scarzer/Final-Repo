## Build Instructions for Supreme Court Viewer
To build this project, you must have node 4.X with npm and bower installed as well
``` bash
    $ export NEO4J_USER=neo4j
    $ export NEO4J_PASS=dataviz
    
    $ npm install
    $ bower install
    $ npm start
```

go to http://localhost:3000 once launched

## Structure of the code

    ├── README.md
    ├── app.js
    ├── bin
    ├── bower.json
    ├── bower_components
    ├── node_modules
    ├── npm-debug.log
    ├── package.json
    ├── public
    ├── routes
    └── views

#### App.js
Main app file as per default ExpressJS set up

#### Bin/
ExpressJS generated binary file for launching application

#### Bower.json and package.json
Project description files along with dependency management of project

#### Public 
Directory for static assets

#### Routes/api.js
Definition of neo4j interfacing API.

#### views/
Default views directory created by ExpressJS. Views here are not being used


