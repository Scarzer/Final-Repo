# The purpose of this repository is to store all of the data worked on by John 
Settineri and Irving. Includes all of the data taken off MongoDB, checks 
for supreme court cases, and basic visualizations used in the beginning.
John Settineri mostly worked on the data mining to find trends, find specific 
cases and their citations, and others, while Irving pushed the initial commits. 
To get it to work, clone this repository and start jupyter-notebook

``` bash
    $ git clone git@gitlab.com:Scarzer/notebooks-for-vis.git
    $ cd notebooks-for-vis
    $ jupyter-notebook
```