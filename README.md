This repository contains the working files for the Antitrust Project.
The final viz can be viewed:
    1. Viewer/public/index.html
    2. Viewer/public/allNodes.html
In this repository you will able to view the slides and final
paper in pdf format.
    1. FinalPaperVisualization.pdf
    2. vispresentation.pdf
This repository was created because the original final repository did not work
properly.


Rubric:


05 # Was the design process documented? -> 3

05 # Is it clear what queries it support? -> 3

05 # Were the visualization types appropriate (scatter, bar, map)? -> 3

05 # How well were visual variables used (marks/channels)? -> 3

05 # Use of color? -> 2

05 # Were visualizations labeled, axis labeled, units? -> 3

05 # Is it a unified application? -> 3

05 # User experience (interactivity) -> 3

05 # Does it run? -> 4

05 # Documentation -> 4

10 # Final Presentation (10pts) -> 2
